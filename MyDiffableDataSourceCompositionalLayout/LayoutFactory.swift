//
//  LayoutFactory.swift
//  MyDiffableDataSourceCompositionalLayout
//
//  Created by Mostafa Sandeed on 3/23/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit

// MARK:  Layout factory
extension ViewController {
    
    // This method will return the appropriate layout based on section (with the aid of the next two methods)
    func createLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionIndex: Int,
          layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            
          let sectionLayoutKind = Section.allCases[sectionIndex]
          switch (sectionLayoutKind) {
              case .male: return self.createLayoutForMales()
              case .female: return self.createLayoutForFemales()
          }
        }
        return layout
    }
    
    func createLayoutForMales() -> NSCollectionLayoutSection {
        
        // Item
        let itemSize = NSCollectionLayoutSize(
          widthDimension: .fractionalWidth(1/3),
          heightDimension: .fractionalHeight(1.0))
        let fullPhotoItem = NSCollectionLayoutItem(layoutSize: itemSize)
        fullPhotoItem.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)

        // Group
        let groupSize = NSCollectionLayoutSize(
          widthDimension: .fractionalWidth(1.0),
          heightDimension: .fractionalWidth(2/3))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: fullPhotoItem, count: 3)

        // Section
        let section = NSCollectionLayoutSection(group: group)
        
        // Section header
        let headerSize = NSCollectionLayoutSize(
          widthDimension: .fractionalWidth(1.0),
          heightDimension: .estimated(44))
        let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
          layoutSize: headerSize,
          elementKind: ViewController.sectionHeaderElementKind,
          alignment: .top)
        section.boundarySupplementaryItems = [sectionHeader]
        
        // Scrolling behavior
        section.orthogonalScrollingBehavior = .groupPaging
        
        return section
    }
    
    func createLayoutForFemales() -> NSCollectionLayoutSection {
        
        // Item
        let itemSize = NSCollectionLayoutSize(
          widthDimension: .fractionalWidth(1/2),
          heightDimension: .fractionalHeight(1.0))
        let fullPhotoItem = NSCollectionLayoutItem(layoutSize: itemSize)
        fullPhotoItem.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)

        // Group
        let groupSize = NSCollectionLayoutSize(
          widthDimension: .fractionalWidth(1.0),
          heightDimension: .fractionalWidth(2/3))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: fullPhotoItem, count: 2)

        // Section
        let section = NSCollectionLayoutSection(group: group)
        
        // Section header
        let headerSize = NSCollectionLayoutSize(
          widthDimension: .fractionalWidth(1.0),
          heightDimension: .estimated(44))
        let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
          layoutSize: headerSize,
          elementKind: ViewController.sectionHeaderElementKind,
          alignment: .top)
        section.boundarySupplementaryItems = [sectionHeader]
        
        // Scrolling behavior
        section.orthogonalScrollingBehavior = .groupPaging
        
        return section
    }
}
