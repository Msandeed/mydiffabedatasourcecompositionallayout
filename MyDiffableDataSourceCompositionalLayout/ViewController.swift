//
//  ViewController.swift
//  MyDiffableDataSourceCompositionalLayout
//
//  Created by Mostafa Sandeed on 3/4/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit

enum Section: String, CaseIterable {
    case male = "Males"
    case female = "Females"
}

fileprivate typealias PersonDataSource = UICollectionViewDiffableDataSource<Section, Person>
fileprivate typealias PersonSnapshot = NSDiffableDataSourceSnapshot<Section, Person>

class ViewController: UIViewController {
    
    static let sectionHeaderElementKind = "section-header-element-kind"
    
    @IBOutlet weak var personsCollectionView: UICollectionView!
    
    fileprivate var dataSource: PersonDataSource!
    
    var persons = [Person(name: "Mostafa", address: "55 Alex str.", gender: .Male),
                   Person(name: "Salma", address: "55 Alex str.", gender: .Female),
                   Person(name: "Ahmed", address: "55 Cairo str.", gender: .Male),
                   Person(name: "Mohamed", address: "55 Hurghada str.", gender: .Male),
                   Person(name: "Dina", address: "55 Cairo str.", gender: .Female),
                   Person(name: "Doha", address: "55 Hurghada str.", gender: .Female),
                   Person(name: "Medhat", address: "55 Alex str.", gender: .Male),
                   Person(name: "Kamal", address: "55 Alex str.", gender: .Male),
                   Person(name: "Omar", address: "55 Alex str.", gender: .Male),
                   Person(name: "Samia", address: "55 Alex str.", gender: .Female),
                   Person(name: "Manal", address: "55 Alex str.", gender: .Female),
                   Person(name: "Samah", address: "55 Alex str.", gender: .Female)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureHierarchy()
        setupDataSource()
    }
    
// MARK: CollectionView basic configuration
    private func configureHierarchy() {
        
        personsCollectionView.register(UINib(nibName: PersonCell.reuseIdentifer, bundle: nil), forCellWithReuseIdentifier: PersonCell.reuseIdentifer)
        
        // For Section header
        personsCollectionView.register(HeaderView.self,
        forSupplementaryViewOfKind: ViewController.sectionHeaderElementKind,
        withReuseIdentifier: HeaderView.reuseIdentifier)
        
        personsCollectionView.collectionViewLayout = createLayout()
    }
    
// MARK: Button actions
    @IBAction func didTapAddBtn(_ sender: UIButton) {
        
        // Add new person
        let newPerson = Person(name: "Adel", address: "44 Some str.", gender: .Male)
        self.persons.append(newPerson)
        self.createSnapshot(from: self.persons)
    }
    
}

// MARK: CollectionView delegate methods
extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

// MARK: DataSource
extension ViewController {
    
    func setupDataSource() {
        
        dataSource = PersonDataSource(collectionView: personsCollectionView) {
            (collectionView: UICollectionView, indexPath: IndexPath, person: Person) -> UICollectionViewCell? in
            
            guard let cell = collectionView.dequeueReusableCell(
              withReuseIdentifier: PersonCell.reuseIdentifer,
              for: indexPath) as? PersonCell else { fatalError("Could not create new cell") }
            cell.populate(with: person)
            return cell
        }
        
        // For section header
        dataSource.supplementaryViewProvider = { (
          collectionView: UICollectionView,
          kind: String,
          indexPath: IndexPath) -> UICollectionReusableView? in

          guard let supplementaryView = collectionView.dequeueReusableSupplementaryView(
            ofKind: kind,
            withReuseIdentifier: HeaderView.reuseIdentifier,
            for: indexPath) as? HeaderView else { fatalError("Cannot create header view") }

          supplementaryView.label.text = Section.allCases[indexPath.section].rawValue
          return supplementaryView
        }
        
        self.createSnapshot(from: self.persons)
    }
    
    func createSnapshot(from persons: [Person]) {
        
        var snapshot = PersonSnapshot()
        
        snapshot.appendSections([.male, .female])
        let males = persons.filter { $0.gender == Gender.Male }
        let females = persons.filter { $0.gender == Gender.Female }
        snapshot.appendItems(males, toSection: .male)
        snapshot.appendItems(females, toSection: .female)
        
        dataSource.apply(snapshot, animatingDifferences: true)
    }
}
