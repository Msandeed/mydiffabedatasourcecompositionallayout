//
//  Person.swift
//  MyDiffableDataSourceCompositionalLayout
//
//  Created by Mostafa Sandeed on 3/4/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

class Person: Hashable {

    let name: String?
    let address: String?
    let gender: Gender?
    
    init(name: String?, address: String?, gender: Gender?) {
        self.name = name
        self.address = address
        self.gender = gender
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }

    static func == (lhs: Person, rhs: Person) -> Bool {
        return lhs.identifier == rhs.identifier
    }

    private let identifier = UUID()

}

enum Gender: String {
    case Male
    case Female
}
