//
//  PersonCell.swift
//  MyDiffableDataSourceCompositionalLayout
//
//  Created by Mostafa Sandeed on 3/4/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell {
    static let reuseIdentifer = "PersonCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func populate(with person: Person) {
        self.nameLabel.text = person.name
        self.addressLabel.text = person.address
        self.genderLabel.text = person.gender.map { $0.rawValue }
    }

}
